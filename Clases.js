function Empleado(nombre, depto) {
    this.nombre = nombre || "";
    this.departamento = depto || "General";
}

function Director(nombre, depto, informes) {
    this.superClase = Empleado;
    this.superClase(nombre, depto);
    this.informes = informes || [];
}

Director.prototype = new Empleado;

function Trabajador(nombre, depto, proyectos) {
    this.superClase = Empleado;
    this.superClase(nombre, depto);
    this.proyectos = proyectos || [];
}

Trabajador.prototype = new Empleado;

function Ingeniero(nombre, proyectos, maquina) {
    this.superClase = Trabajador;
    this.superClase(nombre, "Ingenieria", proyectos);
    this.maquina = maquina;
}

Ingeniero.prototype = new Trabajador;

const nuevoIngeniero = new Ingeniero("Carlos",["Angular","Typescript"],"ERP");

const deptoNuevoIngeniero = nuevoIngeniero.departamento;

console.log(deptoNuevoIngeniero);



